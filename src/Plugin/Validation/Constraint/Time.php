<?php

namespace Drupal\wt_time_constraint\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted text is a valid "H:i" 24-hour time format
 * with leading zeroes
 *
 * @Constraint(
 *   id = "wt_time",
 *   label = @Translation("Time", context = "Validation"),
 *   type = "string"
 * )
 */
class Time extends Constraint {
  public $notTime = '%time is not a valid time, must be between 00:00 and 23:59';
}
