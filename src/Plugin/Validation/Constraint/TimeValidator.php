<?php


namespace Drupal\wt_time_constraint\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Https constraint.
 */
class TimeValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $item = $items->first();
    if (!isset($item)) {
      return NULL;
    }

    foreach ($items as $item) {
      if (preg_match('/^([0-1][0-9]|2[0-3]):[0-5][0-9]$/')) {
        $this->context->addViolation($constraint->notTime, ['%time' => $item->value]);
      }
    }
  }
}
